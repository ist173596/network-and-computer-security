package ist.sirs.group31.smssecure;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import java.sql.SQLException;

import ist.sirs.group31.smssecure.database.FriendDataSource;
import ist.sirs.group31.smssecure.model.Friend;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        //intent goto -> MessageList Activity
        final Intent goToMessageList = new Intent(this, MessageList.class);

        //intent goto -> PeopleList Activity
        final Intent goToPeopleList = new Intent(this, PeopleList.class);

        //intent goto -> Menu Activity
        final Intent gotoMenu = new Intent(this, MenuActivity.class);

        final EditText password_login = (EditText) findViewById(R.id.password_login);
        final TextView password_error= (TextView) findViewById(R.id.password_error);

        Button loginButton = (Button) findViewById(R.id.button_login);
        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (password_login.getText().toString().length() != 0) {
                    Log.i("Info", "Password = " + password_login.getText().toString());
                    password_error.setText("");
                    startActivity(gotoMenu);
                } else {
                    Log.e("Error", "No password inserted");
                    String errorMessage = getString(R.string.NoPasswordErrorMessage);
                    password_error.setText(errorMessage);
                }
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_login, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
