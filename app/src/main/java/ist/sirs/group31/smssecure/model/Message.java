package ist.sirs.group31.smssecure.model;

public class Message implements Comparable<Message>{
    private long ID;
    private String name;
    private String address;
    private long timestamp;
    private String body;

    public Message() { }

    public Message(String address, String body) {
        this.address = address;
        this.body = body;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(long timestamp) {
        this.timestamp = timestamp;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    @Override
    public int compareTo(Message another) {
        // ordena por ordem decrescente de data
        //return this.timestamp.compareTo(another.timestamp);
        if (this.timestamp < another.timestamp)
            return 1;
        if (this.timestamp == another.timestamp)
            return 0;
        return -1;
    }


}
