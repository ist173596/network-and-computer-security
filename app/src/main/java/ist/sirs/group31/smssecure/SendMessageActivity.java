package ist.sirs.group31.smssecure;

import android.app.PendingIntent;
import android.content.Intent;
import android.provider.Telephony;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Random;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import ist.sirs.group31.smssecure.database.FriendDataSource;
import ist.sirs.group31.smssecure.model.Friend;
import ist.sirs.group31.smssecure.security.TimestampGenerator;

public class SendMessageActivity extends AppCompatActivity {
    private EditText message_destination;
    private EditText message_body;
    private FriendDataSource friendsDS;
    private final String separator = "fHx8";
    private final String separator2 = "JCQk";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_send_message);

        String address = null;

        Intent i = getIntent();
        Bundle extras = i.getExtras();
        if (extras != null) {
            address = extras.getString("address");
        }

        friendsDS = new FriendDataSource(this);
        message_destination = (EditText) findViewById(R.id.message_destination);
        if (address != null) message_destination.setText(address);

        message_body = (EditText) findViewById(R.id.message_body);

        Button send_message_button = (Button) findViewById(R.id.send_message_button);
        send_message_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String shortMessage = message_body.getText().toString();
                String destination = message_destination.getText().toString();


                if (!destination.equals("") && !shortMessage.equals("")) {

                    String date = String.valueOf(new Date().getTime());
                    StringBuilder stringToEncrypt = new StringBuilder();
                    stringToEncrypt.append(shortMessage);

                    stringToEncrypt.append(separator); // ||| (!pattern alert!!!)
                    stringToEncrypt.append(date);

                    try {
                        friendsDS.open();

                        Log.i("Info", "StringToEncrypt: " + stringToEncrypt);

                        Friend friend = friendsDS.getFriend(destination);
                        long actualTime = new Date().getTime();
                        if (friend == null) {
                            Toast.makeText(SendMessageActivity.this
                                    , "You have to be a friend of the destination!"
                                    , Toast.LENGTH_SHORT).show();
                            friendsDS.close();
                            return;
                        } else if (friend.getKek() == null) {
                            Toast.makeText(SendMessageActivity.this
                                    , "You have to exchange a secret with your friend first!"
                                    , Toast.LENGTH_SHORT).show();
                            friendsDS.close();
                            return;
                        } else if(friendsDS.getFriend(message_destination.getText().toString()).getKs() == null
                                || Long.parseLong(friendsDS.getFriend(message_destination.getText().toString()).getKsExpiresAt()) == 0
                                || Long.parseLong(friendsDS.getFriend(message_destination.getText().toString()).getKsExpiresAt()) <= actualTime) {

                            shareSecret(destination);
                        }

                        byte[] cryptogram = cipherShortMessage(destination
                                , stringToEncrypt.toString(), false, null);
                        String cryptogramString = Base64.encodeToString(cryptogram
                                , Base64.DEFAULT);
                        String mac = digestShortMessage(cryptogram
                                , Base64.decode(friendsDS.getFriend(destination).getKs()
                                ,Base64.DEFAULT));

                        StringBuilder message = new StringBuilder();
                        message.append(separator);
                        message.append(cryptogramString);
                        message.append(separator);
                        message.append(mac);
                        sendShortMessage(destination, message.toString());
                        Toast.makeText(SendMessageActivity.this, "Message sent"
                                , Toast.LENGTH_LONG).show();
                        Intent i = new Intent(SendMessageActivity.this, MenuActivity.class);
                        startActivity(i);
                        friendsDS.close();

                    } catch (SQLException e) {
                        e.printStackTrace();
                    }
                } else {
                    Toast.makeText(SendMessageActivity.this, "Number or message empty"
                            , Toast.LENGTH_SHORT).show();
                }
            }
        });

        Button cancel_message_button = (Button) findViewById(R.id.cancel_message_button);
        cancel_message_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }

    public void sendShortMessage(String destination, String message) {
        SmsManager smsManager = SmsManager.getDefault();

        PendingIntent sentPendingIntent = PendingIntent.getBroadcast(this, 0
                , new Intent("SMS_SENT"), 0);
        PendingIntent deliveredPendingIntent = PendingIntent.getBroadcast(this, 0
                , new Intent("SMS_DELIVERED"), 0);

        ArrayList<String> smsBodyParts = smsManager.divideMessage(message.toString());
        ArrayList<PendingIntent> sentPendingIntents = new ArrayList<PendingIntent>();
        ArrayList<PendingIntent> deliveredPendingIntents = new ArrayList<PendingIntent>();

        for (int i = 0; i < smsBodyParts.size(); i++) {
            sentPendingIntents.add(sentPendingIntent);
            deliveredPendingIntents.add(deliveredPendingIntent);
        }
        smsManager.sendMultipartTextMessage(destination, null, smsBodyParts
                , sentPendingIntents, deliveredPendingIntents);

    }

    public byte[] cipherShortMessage(String Address, String shortMessage
            , boolean protocol, byte[] iv2) {
        try {
            SecretKey secretKey;
            friendsDS.open();
            String key;
            if(protocol) {
                key = friendsDS.getFriend(Address).getKek();
            }
            else{
                key = friendsDS.getFriend(Address).getKs();
            }

            secretKey = new SecretKeySpec(Base64.decode(key, Base64.DEFAULT),  "AES");

            IvParameterSpec iv;
            if(protocol) {
                iv = new IvParameterSpec(iv2);
            }
            else{
                byte[] ivByte = Base64.decode(this.friendsDS.getFriend(Address).getIv()
                        , Base64.DEFAULT);
                iv = new IvParameterSpec(ivByte);
            }

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.ENCRYPT_MODE, secretKey, iv);

            return cipher.doFinal(shortMessage.getBytes("UTF-8"));

        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException
                | InvalidAlgorithmParameterException | UnsupportedEncodingException
                | BadPaddingException | IllegalBlockSizeException | SQLException e) {
            Log.e("Error", e.getMessage());
        }
        return null;
    }

    public String digestShortMessage(byte[] cryptogram, byte[] key) {
        try {
            SecretKey macKey = new SecretKeySpec(key, 0, key.length, "HmacSHA256");

            Mac mac = Mac.getInstance(macKey.getAlgorithm());
            mac.init(macKey);
            byte[] digest = mac.doFinal(cryptogram);

            return Base64.encodeToString(digest, Base64.DEFAULT);

        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void shareSecret(String address) {
        try {
            friendsDS.open();

            KeyGenerator keygen = KeyGenerator.getInstance("AES");
            keygen.init(256);
            SecretKey secret = keygen.generateKey();

            long timestamp = TimestampGenerator.generateOneWeekDeadline();

            String ks = Base64.encodeToString(secret.getEncoded(), Base64.DEFAULT);
            friendsDS.setKs(friendsDS.getFriend(address), ks, timestamp);

            StringBuilder crypto = new StringBuilder();

            String Ks = friendsDS.getFriend(address).getKs();
            String iv = friendsDS.getFriend(address).getIv();

            String date = String.valueOf(new Date().getTime());
            // Ks || Iv1 || TSks || TSm
            crypto.append(Ks);
            crypto.append(separator);
            crypto.append(iv);
            crypto.append(separator);
            crypto.append(String.valueOf(timestamp));
            crypto.append(separator);
            crypto.append(date);

            byte[] ivBytes = new byte[16];
            new Random().nextBytes(ivBytes);

            byte[] cryptogram = cipherShortMessage(address, crypto.toString(), true, ivBytes);

            String digest = digestShortMessage(cryptogram
                    , Base64.decode(friendsDS.getFriend(address).getKek(), Base64.DEFAULT));

            StringBuilder message = new StringBuilder();    //  $$ {crypto}Kek || Iv2 || Mac
            message.append(separator2);
            message.append(Base64.encodeToString(cryptogram, Base64.DEFAULT));
            message.append(separator);
            message.append(Base64.encodeToString(ivBytes, Base64.DEFAULT));
            message.append(separator);
            message.append(digest);

            sendShortMessage(address, message.toString());

            Log.i("Info", "Mensagem: " + message.toString());
            friendsDS.close();

        } catch (NoSuchAlgorithmException | SQLException e) {
            Toast.makeText(getApplicationContext()
                    , "Failed to create a session key", Toast.LENGTH_SHORT).show();
        }
    }
    
}
