package ist.sirs.group31.smssecure;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import ist.sirs.group31.smssecure.model.Friend;

public class FriendListAdapter extends ArrayAdapter<Friend> {
    private final Context context;
    private final List<Friend> friendList;

    public FriendListAdapter(Context context, List<Friend> friendList) {
        super(context, R.layout.contacts_list_row, friendList);
        this.context = context;
        this.friendList = friendList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.contacts_list_row, parent, false);

        TextView people_name = (TextView) rowView.findViewById(R.id.people_name);
        people_name.setText(friendList.get(position).getName());
        TextView people_number = (TextView) rowView.findViewById(R.id.people_number);
        people_number.setText(friendList.get(position).getAddress());

        return rowView;
    }
}
