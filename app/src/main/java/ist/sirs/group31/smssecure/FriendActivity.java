package ist.sirs.group31.smssecure;

import android.content.Intent;
import android.graphics.Color;
import android.hardware.Camera;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.format.DateUtils;
import android.text.method.KeyListener;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import ist.sirs.group31.smssecure.database.FriendDataSource;
import ist.sirs.group31.smssecure.model.Friend;
import ist.sirs.group31.smssecure.security.TimestampGenerator;

public class FriendActivity extends AppCompatActivity {
    private FriendDataSource friendsDS;
    private Friend friend;
    private SecretKey kek;
    private SecretKey ks;

    private Camera mCamera;
    private CameraPreview mPreview;
    private FrameLayout preview;
    private TextView kek_status;
    private Button show_kek_button;
    private Button save_button;
    private Button ask_secret;
    private EditText kek_display;
    private KeyListener defaultListener;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friend);

        friendsDS = new FriendDataSource(this);

        Intent i = getIntent();
        long ID = i.getExtras().getLong("Id");

        uiSetup();

        try {
            friendsDS.open();
            friend = friendsDS.getFriend(ID);
            friendsDS.close();

            setTitle(friend.getName());
            long actualTime = new Date().getTime();

            final TextView friend_address = (TextView) findViewById(R.id.friend_address);
            friend_address.setText(friend.getAddress());

            if (friend.getKek() != null && !friend.getKek().isEmpty()) {
                byte[] encodedKek = Base64.decode(friend.getKek(), Base64.DEFAULT);
                kek = new SecretKeySpec(encodedKek, 0, encodedKek.length, "AES");
                kek_status.setText("You share a secret with this friend");
                kek_status.setTextColor(Color.rgb(0, 102, 51));
                show_kek_button.setClickable(true);
            } else if (Long.parseLong(friend.getKekExpiresAt()) == 0
                    || Long.parseLong(friend.getKekExpiresAt()) < actualTime) {
                kek_status.setText("Renew the secret with your friend");
                kek_status.setTextColor(Color.RED);
            } else{
                kek_status.setText("No secret shared with this friend");
                kek_status.setTextColor(Color.RED);
            }

            final Button captureButton = (Button) findViewById(R.id.capture_button);
            captureButton.setVisibility(View.INVISIBLE);

            final Button generate_button = (Button) findViewById(R.id.generate_button);
            generate_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // Create an instance of Camera
                    mCamera = getCameraInstance();
                    generate_button.setVisibility(View.INVISIBLE);

                    // Create our Preview view and set it as the content of our activity.
                    mPreview = new CameraPreview(getApplicationContext(), mCamera);
                    preview  = (FrameLayout) findViewById(R.id.camera_preview);
                    preview.addView(mPreview);

                    captureButton.setVisibility(View.VISIBLE);
                    captureButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            // get an image from the camera
                            mCamera.takePicture(null, null, mPicture);
                            generate_button.setVisibility(View.VISIBLE);
                            captureButton.setVisibility(View.INVISIBLE);
                        }
                    });
                    kek_display.setVisibility(View.INVISIBLE);
                }
            });

            final ImageButton send_message_button = (ImageButton) findViewById(R.id.send_message_button);
            send_message_button.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(getApplicationContext(), SendMessageActivity.class);
                    i.putExtra("address", friend_address.getText().toString());
                    startActivity(i);
                }
            });

        } catch(SQLException e) {
            Log.e("Error", "Error to get friend");
            Toast.makeText(this, "Enable to load Friend profile", Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        if (preview != null)
            preview.removeAllViews();
        if (mCamera != null) {
            mCamera.release();
        }
        super.onPause();
    }

    public static Camera getCameraInstance(){
        Camera c = null;
        try {
            c = Camera.open();
        }
        catch (Exception e){
            Log.i("Error", "Camera not available or not existent");
        }
        return c;
    }

    private byte[] xorWithString(byte[] a, byte[] key) {
        byte[] out = new byte[a.length];
        for (int i = 0; i < a.length; i++) {
            out[i] = (byte) (a[i] ^ key[i%key.length]);
        }
        return out;
    }

    private void uiSetup() {
        kek_display = (EditText) findViewById(R.id.kek_display);
        defaultListener = kek_display.getKeyListener();
        kek_display.setVisibility(View.INVISIBLE);

        kek_status = (TextView) findViewById(R.id.kek_status);

        ask_secret = (Button) findViewById(R.id.ask_secret);
        ask_secret.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                kek_display.setKeyListener(defaultListener);
                kek_display.setText("");
                kek_display.setVisibility(View.VISIBLE);
                save_button.setVisibility(View.VISIBLE);
            }
        });

        save_button = (Button) findViewById(R.id.save_button);
        save_button.setVisibility(View.INVISIBLE);
        save_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (kek_display.getText().toString().equals(""))
                        Toast.makeText(getApplicationContext(), "Please insert a valid key", Toast.LENGTH_SHORT).show();
                    else {
                        String newKek = kek_display.getText().toString() + '\n';
                        long timestamp = 1000;
                        friendsDS.open();
                        friendsDS.setKek(friend, newKek, timestamp);
                        friendsDS.close();
                        save_button.setVisibility(View.INVISIBLE);
                        kek_display.setVisibility(View.VISIBLE);
                        friend.setKek(newKek);
                        kek_status.setText("You share a secret with this friend");
                        kek_status.setTextColor(Color.rgb(0, 102, 51));
                        show_kek_button.setClickable(true);
                    }
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        });

        show_kek_button = (Button) findViewById(R.id.show_kek_button);
        show_kek_button.setClickable(false);
        show_kek_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (kek_display.getVisibility() == View.INVISIBLE) {
                    kek_display.setKeyListener(null);
                    kek_display.setText(friend.getKek());
                    kek_display.setVisibility(View.VISIBLE);
                } else {
                    kek_display.setVisibility(View.INVISIBLE);
                    save_button.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    /**
     * Callback for picture taken.
     * This action generates a KEK and an IV from random picture's data bytes
     * mixing them with a random generated key.
     */
    private Camera.PictureCallback mPicture =
            new Camera.PictureCallback() {
                @Override
                public void onPictureTaken(byte[] data, Camera camera) {
                    Log.i("Info", "Picture generated " + data.length + " bytes");
                    try {
                        KeyGenerator keygen = KeyGenerator.getInstance("AES");
                        keygen.init(256);

                        /**
                         * Mixing 16 bytes from the picture with a
                         * random generated key to create a KEK.
                         */
                        byte[] key = new byte[16];
                        for (int i = 0, j = 0; i < 16; i++, j += 5000) key[i] = data[i + j];
                        SecretKey mixingKey = keygen.generateKey();
                        key = xorWithString(key, mixingKey.getEncoded());

                        /**
                         * Mixing 16 bytes from the picture with a
                         * random generated key to create a IV.
                         */
                        byte[] iv = new byte[16];
                        for (int i = 0; i < 16; i++) iv[i] = data[i + 16];
                        SecretKey mixingKey2 = keygen.generateKey();
                        iv = xorWithString(iv, mixingKey2.getEncoded());

                        /**
                         * Generation of a timestamp with a 6 month deadline.
                         */
                        long timestamp = TimestampGenerator.generateSixMonthsDeadline();

                        /**
                         * Setting the Kek for the activity and database.
                         */
                        String kekAsString = Base64.encodeToString(key, Base64.DEFAULT);
                        SecretKey generatedKek = new SecretKeySpec(key, 0, key.length, "AES");
                        kek = generatedKek;
                        kek_status.setText("You share a secret with this friend");
                        kek_status.setTextColor(Color.rgb(0, 102, 51));
                        show_kek_button.setClickable(true);
                        friendsDS.open();
                        friendsDS.setKek(friend, kekAsString, timestamp);

                        /**
                         * Setting the IV for the activity and database.
                         */
                        friendsDS.setIv(friend, Base64.encodeToString(iv, Base64.DEFAULT));
                        friend.setIv(Base64.encodeToString(iv, Base64.DEFAULT));
                        friend.setKek(kekAsString);
                        friend.setKsExpiresAt(String.valueOf(timestamp));

                        friendsDS.close();

                    } catch (SQLException | NoSuchAlgorithmException e) {
                        Log.e("Error", "Failed to set KEK");
                    } finally {
                        preview.removeAllViews();
                        mCamera.stopPreview();
                        mCamera.release();
                    }
                }
            };
}