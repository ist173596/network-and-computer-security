package ist.sirs.group31.smssecure.security;

import java.util.Calendar;
import java.util.Date;

public class TimestampGenerator {

    public TimestampGenerator() { }

    public static long generateOneWeekDeadline() {
        Date actualTime = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(actualTime);
        c.add(Calendar.DAY_OF_MONTH, 7);
        Date sixMonthsDeadline = c.getTime();

        return sixMonthsDeadline.getTime();
    }

    public static long generateSixMonthsDeadline() {
        Date actualTime = new Date();
        Calendar c = Calendar.getInstance();
        c.setTime(actualTime);
        c.add(Calendar.MONTH, 6);
        Date sixMonthsDeadline = c.getTime();

        return sixMonthsDeadline.getTime();
    }
}
