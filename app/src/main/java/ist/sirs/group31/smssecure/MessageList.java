package ist.sirs.group31.smssecure;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.Collections;
import java.util.List;

import ist.sirs.group31.smssecure.database.MessageDataSource;
import ist.sirs.group31.smssecure.model.Message;

public class MessageList extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message_list);
        MessageDataSource messageDS = new MessageDataSource(this);

        ListView list_inbox = (ListView) findViewById(R.id.list_inbox);
        try {
            messageDS.open();
            List<Message> list_messages = messageDS.getAllMessages();
            Collections.sort(list_messages);
            MessageListAdapter adapter = new MessageListAdapter(this, list_messages);
            messageDS.close();
            list_inbox.setAdapter(adapter);
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_message_list, menu);
        return true;
    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MenuActivity.class);
        startActivity(i);
        super.onBackPressed();
    }
}

