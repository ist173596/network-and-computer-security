package ist.sirs.group31.smssecure;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

import ist.sirs.group31.smssecure.database.FriendDataSource;
import ist.sirs.group31.smssecure.model.Friend;

public class MenuActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        Button button_inbox = (Button) findViewById(R.id.button_inbox);
        final Intent goToInbox = new Intent(this, MessageList.class);
        button_inbox.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(goToInbox);
            }
        });

        Button button_add_contact = (Button) findViewById(R.id.button_add_contact);
        final Intent goToAddContact = new Intent(this, PeopleList.class);
        button_add_contact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(goToAddContact);
            }
        });

        Button button_send_message = (Button) findViewById(R.id.button_send_message);
        final Intent goToSendMessage = new Intent(this, SendMessageActivity.class);
        button_send_message.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(goToSendMessage);
            }
        });

        Button button_my_contacts = (Button) findViewById(R.id.button_my_contacts);
        final Intent goToFriendsList = new Intent(this, FriendsList.class);
        button_my_contacts.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(goToFriendsList);
            }
        });
    }
}
