package ist.sirs.group31.smssecure;

import android.content.ContentResolver;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import ist.sirs.group31.smssecure.database.FriendDataSource;
import ist.sirs.group31.smssecure.model.Contact;
import ist.sirs.group31.smssecure.model.Friend;
import ist.sirs.group31.smssecure.model.Message;

public class PeopleList extends AppCompatActivity {
    private List<Contact> listPeople = new ArrayList<>();
    private FriendDataSource friendsDS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_people_list);

        friendsDS = new FriendDataSource(this);
        ListView list_people = (ListView) findViewById(R.id.list_people);

        Uri contactsUri = ContactsContract.Contacts.CONTENT_URI;
        String[] contacts = new String[] { "_id", "has_phone_number", "display_name" };
        ContentResolver cr = getContentResolver();
        Cursor c = cr.query(contactsUri, contacts, null, null, null);

        if (c == null) return;
        for (c.moveToFirst(); !c.isAfterLast(); c.moveToNext()) {
            if (c.getInt(1) == 1) {
                Cursor phones = cr.query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI
                        , null
                        , ContactsContract.CommonDataKinds.Phone.CONTACT_ID + " = " + c.getInt(0)
                        , null, null);
                if (phones != null) phones.moveToFirst();
                String number =
                        phones.getString(phones.getColumnIndex(
                                ContactsContract.CommonDataKinds.Phone.NUMBER));

                listPeople.add(new Contact(c.getString(2), number, 0));

                phones.close();
            }
        }
        c.close();

        Collections.sort(listPeople);
        PeopleListAdapter adapter = new PeopleListAdapter(this, listPeople);
        list_people.setAdapter(adapter);

        list_people.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Contact c = (Contact) parent.getAdapter().getItem(position);
                String name = c.getContactName();
                String address = c.getContactNumber();
                address = address.replaceAll("\\s+", "");
                if (address.charAt(0) != '+') {
                    address = "+351" + address;
                }

                try {
                    friendsDS.open();
                    Friend f = friendsDS.createFriend(name, address);
                    friendsDS.close();
                    Intent goToFriendActivity =
                            new Intent(getApplicationContext(), FriendActivity.class);
                    goToFriendActivity.putExtra("Id", f.getID());
                    startActivity(goToFriendActivity);

                } catch (SQLException e) {
                    Log.e("Error", "Failure creating new Friend");
                }
            }
        });
    }

}
