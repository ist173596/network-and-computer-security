package ist.sirs.group31.smssecure;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import java.sql.SQLException;
import java.util.List;

import ist.sirs.group31.smssecure.database.FriendDataSource;
import ist.sirs.group31.smssecure.model.Friend;

public class FriendsList extends AppCompatActivity {
    private FriendDataSource friendsDS;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends_list);

        ListView friends_list = (ListView) findViewById(R.id.friends_list);

        friendsDS = new FriendDataSource(this);
        try {
            friendsDS.open();
            List<Friend> friends = friendsDS.getAllFriends();

            Log.i("Info", "Friends fetched: " + friends.size());
            friendsDS.close();

            FriendListAdapter adapter = new FriendListAdapter(this, friends);
            friends_list.setAdapter(adapter);

            friends_list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Friend f = (Friend) parent.getAdapter().getItem(position);
                    Intent goToFriendActivity = new Intent(getApplicationContext()
                            , FriendActivity.class);
                    goToFriendActivity.putExtra("Id", f.getID());
                    startActivity(goToFriendActivity);
                }
            });
        } catch (SQLException e) {
            Log.e("Error", e.getMessage());
        } finally {
            friendsDS.close();
        }
    }
}