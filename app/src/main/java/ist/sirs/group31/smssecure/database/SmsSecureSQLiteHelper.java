package ist.sirs.group31.smssecure.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SmsSecureSQLiteHelper  extends SQLiteOpenHelper{
    //Friends Table
    public static final String friendsTable = "friends";
    public static final String friendsTableColumnId = "ID";
    public static final String friendsTableColumnName = "name";
    public static final String friendsTableColumnAddress = "address";
    public static final String friendsTableColumnIv = "iv";
    public static final String friendsTableColumnKek = "kek";
    public static final String friendsTableColumnKekExpiresAt = "kek_expires_at";
    public static final String friendsTableColumnKs = "ks";
    public static final String friendsTableColumnKsExpiresAt = "ks_expires_at";

    //Messages Table
    public static final String messagesTable = "messages";
    public static final String messagesTableColumnId = "ID";
    public static final String messagesTableColumnName = "name";
    public static final String messagesTableColumnAddress = "address";
    public static final String messagesTableColumnTimestamp = "timestamp";
    public static final String messagesTableColumnBody = "body";

    public static final String databaseName = "SmsSecureDB";
    public static final int databaseVersion = 10;

    private static final String createFriendsTable =
            "create table " + friendsTable + " ("
            + friendsTableColumnId + " integer primary key autoincrement, "
            + friendsTableColumnName + " text unique not null, "
            + friendsTableColumnAddress + " text unique not null, "
            + friendsTableColumnIv + " text, "
            + friendsTableColumnKek + " text, "
            + friendsTableColumnKekExpiresAt + " text default '0', "
            + friendsTableColumnKs + " text, "
            + friendsTableColumnKsExpiresAt + " text default '0');";

    private static final String createMessagesTable =
            "create table " + messagesTable + " ("
            + messagesTableColumnId + " integer primary key autoincrement, "
            + messagesTableColumnName + " text, "
            + messagesTableColumnAddress + " text not null, "
            + messagesTableColumnTimestamp + " integer, "
            + messagesTableColumnBody + " text not null);";

    public SmsSecureSQLiteHelper(Context context) {
        super(context, databaseName, null, databaseVersion);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createFriendsTable);
        db.execSQL(createMessagesTable);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.i("Info", "Upgrading database from version "
                + oldVersion + " to new version " + newVersion);
        db.execSQL("drop table if exists " + friendsTable + ";");
        db.execSQL("drop table if exists " + messagesTable + ";");
        onCreate(db);
    }
}
