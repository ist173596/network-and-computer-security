package ist.sirs.group31.smssecure;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Calendar;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyGenerator;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;

import ist.sirs.group31.smssecure.database.MessageDataSource;
import ist.sirs.group31.smssecure.model.Message;

public class OpenMessageActivity extends AppCompatActivity {
    private String message_origin;
    private String message_body;
    private long message_date;
    private String message_name;
    public long id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_message);

        Bundle extras = getIntent().getExtras();
        if (extras == null) {
            this.message_origin = null;
            this.message_body = null;
            this.message_name = null;
            this.message_date = 0;
            this.id = 0;
        } else {
            this.message_origin = extras.getString("number");
            this.message_body = extras.getString("message");
            this.message_date = extras.getLong("date");
            this.message_name = extras.getString("name");
            this.id = extras.getLong("id");
        }

        final TextView read_message_body = (TextView) findViewById(R.id.read_message_body);
        final TextView read_message_sender = (TextView) findViewById(R.id.read_message_sender);
        final TextView messageDate = (TextView) findViewById(R.id.message_date);

        read_message_sender.setText(this.message_name);
        read_message_body.setText(this.message_body);

        String dateString= DateFormat.format("dd/MM/yyyy HH:mm", new Date(message_date)).toString();
        messageDate.setText(dateString);

        Button reply_message_button = (Button) findViewById(R.id.reply_message_button);
        reply_message_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), SendMessageActivity.class);
                i.putExtra("address", message_origin);
                startActivity(i);
            }
        });

        Button delete_message_button = (Button) findViewById(R.id.delete_message_button);
        delete_message_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MessageDataSource messageDS = new MessageDataSource(getApplicationContext());
                try {
                    messageDS.open();
                    messageDS.deleteMessage(messageDS.getMessage(id));
                    messageDS.close();

                    final Intent goToInbox = new Intent(getApplicationContext(), MessageList.class);
                    startActivity(goToInbox);


                } catch (SQLException e) {
                    e.printStackTrace();
                }

            }
        });

    }

    @Override
    public void onBackPressed() {
        Intent i = new Intent(this, MessageList.class);
        startActivity(i);
        super.onBackPressed();
    }
}