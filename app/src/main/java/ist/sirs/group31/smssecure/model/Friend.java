package ist.sirs.group31.smssecure.model;

public class Friend {
    private long ID;
    private String name;
    private String address;
    private String kek;
    private String kekExpiresAt;
    private String ks;
    private String ksExpiresAt;
    private String iv;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getKek() {
        return kek;
    }

    public void setKek(String kek) {
        this.kek = kek;
    }

    public String getKs() {
        return ks;
    }

    public void setKs(String ks) {
        this.ks = ks;
    }

    public long getID() {
        return ID;
    }

    public void setID(long ID) {
        this.ID = ID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    public String getKekExpiresAt() {
        return kekExpiresAt;
    }

    public void setKekExpiresAt(String kekExpiresAt) {
        this.kekExpiresAt = kekExpiresAt;
    }

    public String getKsExpiresAt() {
        return ksExpiresAt;
    }

    public void setKsExpiresAt(String ksExpiresAt) {
        this.ksExpiresAt = ksExpiresAt;
    }
}
