package ist.sirs.group31.smssecure;

import android.content.Context;
import android.content.Intent;
import android.text.format.DateFormat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Date;
import java.util.List;

import ist.sirs.group31.smssecure.model.Message;

public class MessageListAdapter extends ArrayAdapter<Message> {
    private final Context context;
    private final List<Message> messageList;

    public MessageListAdapter(Context context, List<Message> messageList) {
        super(context, R.layout.messages_list_row, messageList);
        this.context = context;
        this.messageList = messageList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.messages_list_row, parent, false);

        TextView message_number = (TextView) rowView.findViewById(R.id.message_number);
        message_number.setText(messageList.get(position).getName());

        TextView message_preview = (TextView) rowView.findViewById(R.id.message_preview);
        message_preview.setText(messageList.get(position).getBody());

        TextView message_date = (TextView) rowView.findViewById(R.id.message_date);
        message_date.setText(DateFormat.format("dd/MM/yyyy HH:mm"
                , new Date(messageList.get(position).getTimestamp())).toString());


        final Intent goToOpenMessage = new Intent(this.context, OpenMessageActivity.class);
        goToOpenMessage.putExtra("number", messageList.get(position).getAddress());
        goToOpenMessage.putExtra("name", messageList.get(position).getName());
        goToOpenMessage.putExtra("message", messageList.get(position).getBody());
        goToOpenMessage.putExtra("date", messageList.get(position).getTimestamp());
        goToOpenMessage.putExtra("id", messageList.get(position).getID());
        rowView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                context.startActivity(goToOpenMessage);
            }
        });

        return rowView;
    }


}
