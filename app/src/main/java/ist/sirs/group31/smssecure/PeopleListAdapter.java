package ist.sirs.group31.smssecure;

import android.content.Context;
import android.media.Image;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.Collections;
import java.util.List;

import ist.sirs.group31.smssecure.model.Contact;
import ist.sirs.group31.smssecure.model.Message;

public class PeopleListAdapter extends ArrayAdapter<Contact> {
    private final Context context;
    private final List<Contact> contactList;

    public PeopleListAdapter(Context context, List<Contact> contactList) {
        super(context, R.layout.contacts_list_row, contactList);
        this.context = context;
        this.contactList = contactList;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater =
                (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.contacts_list_row, parent, false);

        TextView people_name = (TextView) rowView.findViewById(R.id.people_name);
        people_name.setText(contactList.get(position).getContactName());
        TextView people_number = (TextView) rowView.findViewById(R.id.people_number);
        people_number.setText(contactList.get(position).getContactNumber());

        return rowView;
    }
}
