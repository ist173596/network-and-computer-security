package ist.sirs.group31.smssecure.model;

public class Contact implements Comparable<Contact>{
    private String contactName;
    private String contactNumber;
    private int contactImageThumbnail;
    private String secret;

    public Contact(String contactName, String contactNumber, int contactImageThumbnail
                   , String secret) {
        this.contactName = contactName;
        this.contactNumber = contactNumber;
        this.contactImageThumbnail = contactImageThumbnail;
        this.secret = secret;
    }

    public Contact(String contactName, String contactNumber, int contactImageThumbnail) {
        this.contactName = contactName;
        this.contactNumber = contactNumber;
        this.contactImageThumbnail = contactImageThumbnail;
    }

    public String getContactName() {
        return contactName;
    }

    public String getContactNumber() {
        return contactNumber;
    }

    public int getContactImageThumbnail() {
        return contactImageThumbnail;
    }

    public String getSecret() throws Exception {
        if (secret != null)
            return secret;
        else
            throw new Exception(); //TODO - We have to create an exception of our own.
    }


    @Override
    public int compareTo(Contact another) {
       return this.contactName.compareTo(another.contactName);
    }
}
