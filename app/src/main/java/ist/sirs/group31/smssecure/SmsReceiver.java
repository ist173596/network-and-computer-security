package ist.sirs.group31.smssecure;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.telephony.SmsMessage;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;

import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.sql.SQLException;
import java.util.Date;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.Mac;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import ist.sirs.group31.smssecure.database.FriendDataSource;
import ist.sirs.group31.smssecure.database.MessageDataSource;

public class SmsReceiver extends BroadcastReceiver {
    private final String separator = "fHx8";
    private final String separator2 = "JCQk";
    private FriendDataSource friendsDS;
    private MessageDataSource messagesDS;

    /**
     * This function behaves as a listener intercepting messages and reacting
     * based in their content.
     * @param context
     * @param intent
     */
    @Override
    public void onReceive(Context context, Intent intent) {
        // Get the data (SMS data) bound to intent
        Bundle bundle = intent.getExtras();
        FriendDataSource friendsDS = new FriendDataSource(context);
        MessageDataSource messagesDS = new MessageDataSource(context);

        SmsMessage[] msgs = null;

        String str = "";

        if (bundle != null) {
            Object[] pdus = (Object[]) bundle.get("pdus");
            msgs = new SmsMessage[pdus.length];

            for (int i=0; i < msgs.length; i++) {
                msgs[i] = SmsMessage.createFromPdu((byte[]) pdus[i]);
                str += msgs[i].getMessageBody().toString();
            }

            String address = msgs[0].getOriginatingAddress();

            /**
             * Messages from SecureSMS have at least 4 charactes, so every other messages
             * are instantly dropped.
             */
            if(str.length() > 3) {
                try {
                    friendsDS.open();
                    /**
                     * Messages that use the 'separator' as prefix are common messages, i.e.,
                     * messages with text content.
                     */
                    if (str.substring(0, 4).equals(separator)) {
                        if (friendsDS.getFriend(address) == null) {
                            Toast.makeText(context, address + " is trying to text you but "
                                    + " you are not friends in SMSSecure application"
                                    , Toast.LENGTH_LONG);
                            return;
                        } else if (friendsDS.getFriend(address).getKs() == null) {
                            Toast.makeText(context, address + " is trying to text you but "
                                    + " you are don't share a session key"
                                    , Toast.LENGTH_LONG);
                            return;
                        }
                        String decMessage = decipherMessage(address, str, friendsDS);
                        if (decMessage != null) {
                            messagesDS.open();
                            messagesDS.createMessage(friendsDS.getFriend(address).getName()
                                    , address, new Date().getTime(), decMessage);
                            messagesDS.close();
                            Log.i("Info", "Recebi a mensagem: \"" + str + "\"");

                            /**
                             * Building a notification for message reception.
                             * Messages that do not start with 'separator' are ignored by
                             * the notifications.
                             */
                            Intent i = new Intent(context, MessageList.class);
                            PendingIntent pIntent = PendingIntent.getActivity(context
                                    , (int) System.currentTimeMillis(), i, 0);

                            NotificationCompat.Builder mBuilder =
                                    new NotificationCompat.Builder(context)
                                            .setSmallIcon(R.mipmap.logo)
                                            .setContentTitle("New Message")
                                            .setContentText(
                                                    friendsDS.getFriend(address).getName())
                                            .setContentIntent(pIntent)
                                            .setAutoCancel(true);
                            NotificationManager mNotificationManager =
                                    (NotificationManager) context.getSystemService(
                                            Context.NOTIFICATION_SERVICE);
                            mNotificationManager.notify(1, mBuilder.build());

                            Toast.makeText(context, "You received a message!"
                                    , Toast.LENGTH_SHORT).show();
                        } else {
                            Toast.makeText(context, "The message has been compromised!"
                                    , Toast.LENGTH_SHORT).show();
                        }
                    }
                    /**
                     * Messages that use the 'separator2' as prefix are common session key
                     * renewals messages.
                     */
                    else if (str.substring(0, 4).equals(separator2)) {
                        //if(receiveSecret(str, address, friendsDS)==null)
                        String[] parts = str.substring(4, str.length()).split(separator);

                        /**
                         * Retrieve the session key, IV and deadline for session key
                         * expiration ciphered with the KEK, the IV to decipher this and
                         * a MAC generated with the KEK.
                         */
                        String cryptogram = parts[0]; // { Ks || Iv1 || Timestamp }Kek
                        String iv2 = parts[1];  //iv (plain text)
                        String mac = parts[2];  //mac (plain text)

                        byte[] ivByte = Base64.decode(iv2, Base64.DEFAULT);
                        IvParameterSpec iv = new IvParameterSpec(ivByte);

                        byte[] Kek = Base64.decode(friendsDS.getFriend(address).getKek()
                                , Base64.DEFAULT);
                        SecretKey secretKey = new SecretKeySpec(Kek
                                , 0, Kek.length, "AES/CBC/PKCS5Padding");

                        /**
                         * Check MAC integrity. If the MAC generated does
                         * not match the MAC in the message, authenticity and
                         * integrity have been broken.
                         */
                        String macGenerated = digestShortMessage(Base64.decode(cryptogram
                                , Base64.DEFAULT), Kek);
                        if(!mac.equals(macGenerated)){
                            Toast.makeText(context, "The protocol message has been compromised!"
                                    , Toast.LENGTH_SHORT).show();
                            return;
                        }

                        Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");

                        Log.i("Info", "IV: " + Base64.encodeToString(ivByte, Base64.DEFAULT));
                        cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
                        byte[] plaintext =
                                cipher.doFinal(Base64.decode(cryptogram, Base64.DEFAULT));

                        /**
                         * Retrieve the session key, IV and deadline for session key
                         * expiration that resulted from decrypting the message with
                         * the KEK and IV.
                         */
                        String[] res = (new String(plaintext, "UTF-8")).split(separator);
                        String Ks = res[0];
                        String Iv = res[1];
                        Log.i("Info", "IV1: " + iv);
                        long timestamp = Long.parseLong(res[2]);
                        long messageTime = Long.parseLong(res[3]);
                        if (messageTime < messageTime + 60000
                                && messageTime > messageTime - 60000) {
                            friendsDS.setKs(friendsDS.getFriend(address), Ks, timestamp);
                            friendsDS.setIv(friendsDS.getFriend(address), Iv);
                        } else {
                            Toast.makeText(context, "You received an old message"
                                    , Toast.LENGTH_SHORT).show();
                        }

                    }
                } catch (SQLException | NoSuchPaddingException | InvalidKeyException
                        | BadPaddingException | IllegalBlockSizeException
                        | NoSuchAlgorithmException | InvalidAlgorithmParameterException
                        | UnsupportedEncodingException e) {
                    e.printStackTrace();
                } finally {
                    friendsDS.close();
                }
            }
        }
    }

    public String decipherMessage(String Address, String message, FriendDataSource friendsDS) {
        try {
            String[] parts = message.split(separator);
            String cryptogram = parts[1]; // {message|||date}Ks
            String mac = parts[2];  //mac (plain text)

            byte[] ivByte = Base64.decode(friendsDS.getFriend(Address).getIv(), Base64.DEFAULT);
            byte[] Ks = Base64.decode(friendsDS.getFriend(Address).getKs(), Base64.DEFAULT);

            SecretKey secretKey = new SecretKeySpec(Ks, 0, Ks.length, "AES/CBC/PKCS5Padding");
            String macGenerated = digestShortMessage(Base64.decode(cryptogram, Base64.DEFAULT), Ks);

            if(!mac.equals(macGenerated)) return null;

            IvParameterSpec iv = new IvParameterSpec(ivByte);

            Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            cipher.init(Cipher.DECRYPT_MODE, secretKey, iv);
            byte[] plaintext = cipher.doFinal(Base64.decode(cryptogram, Base64.DEFAULT));

            String[] res = (new String(plaintext, "UTF-8")).split(separator);
            return res[0];

        } catch (NoSuchPaddingException | InvalidKeyException | BadPaddingException
                | IllegalBlockSizeException | NoSuchAlgorithmException
                | InvalidAlgorithmParameterException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        return null;
    }

    public String digestShortMessage(byte[] cryptogram, byte[] key) {
        try {
            SecretKey macKey = new SecretKeySpec(key, 0, key.length, "HmacSHA256");

            Mac mac = Mac.getInstance(macKey.getAlgorithm());
            mac.init(macKey);
            byte[] digest = mac.doFinal(cryptogram);

            return Base64.encodeToString(digest, Base64.DEFAULT);

        } catch (NoSuchAlgorithmException | InvalidKeyException e) {
            e.printStackTrace();
        }
        return null;
    }
}