package ist.sirs.group31.smssecure.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ist.sirs.group31.smssecure.model.Friend;
import ist.sirs.group31.smssecure.model.Message;

public class MessageDataSource {
    private SQLiteDatabase db;
    private SmsSecureSQLiteHelper dbHelper;
    private String[] messagesTableColumns = {
            SmsSecureSQLiteHelper.messagesTableColumnId,
            SmsSecureSQLiteHelper.messagesTableColumnName,
            SmsSecureSQLiteHelper.messagesTableColumnAddress,
            SmsSecureSQLiteHelper.messagesTableColumnTimestamp,
            SmsSecureSQLiteHelper.messagesTableColumnBody
    };

    public MessageDataSource(Context context) {
        dbHelper = new SmsSecureSQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Message createMessage(String name, String address, long timestamp, String body) {
        ContentValues cv = new ContentValues();
        cv.put(SmsSecureSQLiteHelper.messagesTableColumnName, name);
        cv.put(SmsSecureSQLiteHelper.messagesTableColumnAddress, address);
        cv.put(SmsSecureSQLiteHelper.messagesTableColumnTimestamp, timestamp);
        cv.put(SmsSecureSQLiteHelper.messagesTableColumnBody, body);
        long ID = db.insert(SmsSecureSQLiteHelper.messagesTable, null, cv);

        Cursor c = db.query(SmsSecureSQLiteHelper.messagesTable
                , messagesTableColumns
                , SmsSecureSQLiteHelper.messagesTableColumnId + " = " + ID
                , null, null, null, null);
        c.moveToFirst();

        Message m = cursorToMessage(c);
        c.close();

        return m;
    }

    public void deleteMessage(Message message) {
        long ID = message.getID();
        db.delete(SmsSecureSQLiteHelper.messagesTable
                , SmsSecureSQLiteHelper.messagesTableColumnId + " = " + ID
                , null);
    }


    public Message getMessage(long id) {
        Cursor c = db.query(SmsSecureSQLiteHelper.messagesTable
                , messagesTableColumns
                , SmsSecureSQLiteHelper.messagesTableColumnId + " = " + id
                , null, null, null, null);
        c.moveToFirst();

        Message m = cursorToMessage(c);
        return m;
    }

    public List<Message> getAllMessages() {
        List<Message> messages = new ArrayList<>();

        Cursor cursor = db.query(SmsSecureSQLiteHelper.messagesTable
                , messagesTableColumns
                , null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Message m = cursorToMessage(cursor);
            messages.add(m);
            cursor.moveToNext();
        }
        cursor.close();

        return messages;
    }

    public Message cursorToMessage(Cursor cursor) {
        Message m = new Message();
        m.setID(cursor.getLong(0));
        m.setName(cursor.getString(1));
        m.setAddress(cursor.getString(2));
        m.setTimestamp(cursor.getLong(3));
        m.setBody(cursor.getString(4));

        return m;
    }
}
