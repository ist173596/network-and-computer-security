package ist.sirs.group31.smssecure.database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import ist.sirs.group31.smssecure.model.Friend;

public class FriendDataSource {
    private SQLiteDatabase db;
    private SmsSecureSQLiteHelper dbHelper;
    private String[] friendsTableColumns = {
            SmsSecureSQLiteHelper.friendsTableColumnId,
            SmsSecureSQLiteHelper.friendsTableColumnName,
            SmsSecureSQLiteHelper.friendsTableColumnAddress,
            SmsSecureSQLiteHelper.friendsTableColumnIv,
            SmsSecureSQLiteHelper.friendsTableColumnKek,
            SmsSecureSQLiteHelper.friendsTableColumnKekExpiresAt,
            SmsSecureSQLiteHelper.friendsTableColumnKs,
            SmsSecureSQLiteHelper.friendsTableColumnKsExpiresAt
    };

    public FriendDataSource(Context context) {
        dbHelper = new SmsSecureSQLiteHelper(context);
    }

    public void open() throws SQLException {
        db = dbHelper.getWritableDatabase();
    }

    public void close() {
        dbHelper.close();
    }

    public Friend createFriend(String name, String address) {
        ContentValues cv = new ContentValues();
        cv.put(SmsSecureSQLiteHelper.friendsTableColumnName, name);
        cv.put(SmsSecureSQLiteHelper.friendsTableColumnAddress, address);
        long ID = db.insert(SmsSecureSQLiteHelper.friendsTable, null, cv);

        Cursor c = db.query(SmsSecureSQLiteHelper.friendsTable
                , friendsTableColumns
                , SmsSecureSQLiteHelper.friendsTableColumnId + " = " + ID
                , null, null, null, null);
        c.moveToFirst();

        Friend f = cursorToFriend(c);
        c.close();

        return f;
    }

    public void deleteFriend(Friend friend) {
        long ID = friend.getID();
        db.delete(SmsSecureSQLiteHelper.friendsTable
                , SmsSecureSQLiteHelper.friendsTableColumnId + " = " + ID
                , null);
    }

    public Friend getFriend(long id) {
        Cursor c = db.query(SmsSecureSQLiteHelper.friendsTable
                , friendsTableColumns
                , SmsSecureSQLiteHelper.friendsTableColumnId + " = " + id
                , null, null, null, null);
        c.moveToFirst();

        Friend f = cursorToFriend(c);
        c.close();
        return f;
    }

    public Friend getFriend(String address) {
        if(!(address.substring(0,4).equals("+351"))) address = "+351"+address;
        Cursor c = db.query(SmsSecureSQLiteHelper.friendsTable
                , friendsTableColumns
                , SmsSecureSQLiteHelper.friendsTableColumnAddress + " = '" + address + "'"
                , null, null, null, null);
        if (c == null || !c.moveToFirst()) {
            c.close();
            return null;
        } else {
            Friend f = cursorToFriend(c);
            c.close();
            return f;
        }
    }

    public void setKek(Friend f, String kek, long timestamp) {
        ContentValues cv = new ContentValues();
        cv.put(SmsSecureSQLiteHelper.friendsTableColumnKek, kek);
        cv.put(SmsSecureSQLiteHelper.friendsTableColumnKekExpiresAt, timestamp);
        db.update(SmsSecureSQLiteHelper.friendsTable, cv
                , SmsSecureSQLiteHelper.friendsTableColumnId + " = " + f.getID(), null);
    }

    public void setIv(Friend f, String iv) {
        ContentValues cv = new ContentValues();
        cv.put(SmsSecureSQLiteHelper.friendsTableColumnIv, iv);
        db.update(SmsSecureSQLiteHelper.friendsTable, cv
                , SmsSecureSQLiteHelper.friendsTableColumnId + " = " + f.getID(), null);
    }

    public void setKs(Friend f, String ks, long timestamp) {
        ContentValues cv = new ContentValues();
        cv.put(SmsSecureSQLiteHelper.friendsTableColumnKs, ks);
        cv.put(SmsSecureSQLiteHelper.friendsTableColumnKsExpiresAt, timestamp);
        db.update(SmsSecureSQLiteHelper.friendsTable, cv
                , SmsSecureSQLiteHelper.friendsTableColumnId + " = " + f.getID(), null);
    }


    public List<Friend> getAllFriends() {
        List<Friend> friends = new ArrayList<>();

        Cursor cursor = db.query(SmsSecureSQLiteHelper.friendsTable
                , friendsTableColumns
                , null, null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            Friend f = cursorToFriend(cursor);
            friends.add(f);
            cursor.moveToNext();
        }
        cursor.close();

        return friends;
    }

    public Friend cursorToFriend(Cursor cursor) {
        Friend f = new Friend();
        f.setID(cursor.getInt(0));
        f.setName(cursor.getString(1));
        f.setAddress(cursor.getString(2));
        f.setIv(cursor.getString(3));
        f.setKek(cursor.getString(4));
        f.setKekExpiresAt(cursor.getString(5));
        f.setKs(cursor.getString(6));
        f.setKsExpiresAt(cursor.getString(7));

        return f;
    }
}
